/**
 * @file
 * Adds the ability to filter the list based on entered text.
 */

(function ($) {

  Drupal.behaviors.uw_ct_service = {
    attach: function (context, settings) {
      if (!$('#a-z-filter').length) {
        $('#content .view-header').after('<div><input type="text" id="a-z-filter" placeholder="A-Z filter: enter service name"></div><div id="a-z-nomatch">No matching services.</div>');
        $('#a-z-filter').on('keyup', function () {
          filter = $(this).val().toLowerCase();
          if (filter == '') {
            $('#block-system-main .view-display-id-service_all_attachment .views-row, .view-display-id-service_glossary_attachment, #block-system-main .view-display-id-service_all_attachment .view-content h3').show();
            $('#a-z-nomatch').hide();
          }
          else {
            $('#block-system-main .view-display-id-service_all_attachment .views-row, .view-display-id-service_glossary_attachment, #block-system-main .view-display-id-service_all_attachment .view-content h3').hide();
            $('#block-system-main .view-display-id-service_all_attachment .views-row').filter(function () {
              text = $(this).find('a').html().toLowerCase();
              return text.indexOf(filter) > -1;
            }).show().each(function () {
              $(this).prevAll('h3').first().show();
            });
            if ($('#block-system-main .view-display-id-service_all_attachment .views-row:visible').length) {
              $('#a-z-nomatch').hide();
            }
            else {
              $('#a-z-nomatch').show();
            }
          }
        });
      }
    }
  };

}(jQuery));
