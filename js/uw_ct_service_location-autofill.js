/**
 * @file
 * Adds the ability to select a pre-existing location.
 *
 * This makes an AJAX call to populate the data from a central location.
 */

(function ($) {

  Drupal.behaviors.uw_ct_service = {
    attach: function (context, settings) {
     if ($("#autofill-location-data").length === 0) {
      $locations = $('<select>').addClass('form-select').attr('id', settings.uw_ct_service.selectId);

      // Create the list of locations.
      if (settings.uw_ct_service.data) {
        // Add first location placeholder.
        $locations.append($('<option>').text(settings.uw_ct_service.custom).attr('value', 'custom'));

        // Add location selections.
        $.each(settings.uw_ct_service.data, function (key, value) {
          $locations.append($('<option>').text(value.name).attr('value', key));
        });

        // Add the list to the page.
        $locations.insertBefore('#' + settings.uw_ct_service.containerId, context)
        $locations.wrap($('<div>').addClass('form-item'));
        $locations.before($('<label>').text(settings.uw_ct_service.label).attr('for', settings.uw_ct_service.selectId));
        $locations.after($('<div>').text(settings.uw_ct_service.description).addClass('description'));

        // Set location data when the list changes.
        $locations.change(function () {
          if ($locations.val() != 'custom') {
            _populate_location_data(settings.uw_ct_service.data[$locations.val()])
          }
          else {
            _populate_location_data(
              {
                name: '',
                additional: '',
                street: '',
                city: '',
                province: '',
                postal_code: '',
                country: 'ca',
                latitude: '',
                longitude: ''
              }
            );
          }
        });
      }
     }
    }
  };

  // TODO: use a variable setting from Drupal for the field name / language id selector.
  function _populate_location_data(data) {
    $('#edit-field-service-location-und-0-name').val(data.name);
    $('#edit-field-service-location-und-0-additional').val(data.additional);
    $('#edit-field-service-location-und-0-street').val(data.street);
    $('#edit-field-service-location-und-0-city').val(data.city);
    $('#edit-field-service-location-und-0-province').val(data.province);
    $('#edit-field-service-location-und-0-postal-code').val(data.postal_code);
    $('#edit-field-service-location-und-0-country').val(data.country);
    $('#edit-field-service-location-und-0-locpick-user-latitude').val(data.latitude);
    $('#edit-field-service-location-und-0-locpick-user-longitude').val(data.longitude);
  }

}(jQuery));
