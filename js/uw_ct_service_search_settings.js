/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * This js file allows site manager to select Put service search on the sidebar of homepage.
 */

(function ($) {
  $(function () {
    // Page Load:
    // The checkbox of service search is checked, it shows title and description fields.
    if ($('.form-item-search-settings input').is(':checked')) {
      $('.form-item-search-settings-title').show();
      $('.form-item-search-settings-description').show();
    }
    // The checkbox of service search is unchecked, it hides title and description fields and empty values of them.
    else {
      $('.form-item-search-settings-title').hide().find('input').val('Services search');
      $('.form-item-search-settings-description').hide().find('textarea').val('');
    }
    // Checkbox is clicked:
    $('.form-item-search-settings input').click(function () {
      if ($('.form-item-search-settings input').is(':checked')) {
        $('.form-item-search-settings-title').show('Service search');
        $('.form-item-search-settings-description').show();
      }
      else {
        $('.form-item-search-settings-title').hide().find('input').val('');
        $('.form-item-search-settings-description').hide().find('textarea').val('');
      }
    });
  });
})(jQuery);
