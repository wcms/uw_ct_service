<?php

/**
 * @file
 * uw_ct_service.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_service_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_service-ist-settings:admin/config/system/uw_ct_service_ist_settings.
  $menu_links['menu-site-management_service-ist-settings:admin/config/system/uw_ct_service_ist_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_ct_service_ist_settings',
    'router_path' => 'admin/config/system/uw_ct_service_ist_settings',
    'link_title' => 'Service IST settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_service-ist-settings:admin/config/system/uw_ct_service_ist_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-management_service-settings:admin/config/system/uw_ct_service_settings.
  $menu_links['menu-site-management_service-settings:admin/config/system/uw_ct_service_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_ct_service_settings',
    'router_path' => 'admin/config/system/uw_ct_service_settings',
    'link_title' => 'Service settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_service-settings:admin/config/system/uw_ct_service_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_service-categories:admin/structure/taxonomy/service_categories.
  $menu_links['menu-site-manager-vocabularies_service-categories:admin/structure/taxonomy/service_categories'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/service_categories',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Service categories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_service-categories:admin/structure/taxonomy/service_categories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_service-tags:admin/structure/taxonomy/uw_service_tags.
  $menu_links['menu-site-manager-vocabularies_service-tags:admin/structure/taxonomy/uw_service_tags'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_service_tags',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Service tags',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_service-tags:admin/structure/taxonomy/uw_service_tags',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Service IST settings');
  t('Service categories');
  t('Service settings');
  t('Service tags');

  return $menu_links;
}
