<?php

/**
 * @file
 * uw_ct_service.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function uw_ct_service_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'services_service_content';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/v1/services';
  $endpoint->authentication = array(
    'services_api_key_auth' => array(
      'api_key' => '96ab9383e6ad48c23aa1504dc9cc5c52',
      'api_key_source' => 'request',
      'user' => 'WCMS web service user',
    ),
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'bencode' => TRUE,
      'json' => TRUE,
      'php' => TRUE,
      'xml' => TRUE,
      'jsonp' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/xml' => TRUE,
      'multipart/form-data' => TRUE,
      'text/xml' => TRUE,
      'application/vnd.php.serialized' => FALSE,
      'application/x-www-form-urlencoded' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'service_content' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['services_service_content'] = $endpoint;

  return $export;
}
