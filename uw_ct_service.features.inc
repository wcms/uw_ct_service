<?php

/**
 * @file
 * uw_ct_service.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_service_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_service_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_service_image_default_styles() {
  $styles = array();

  // Exported image style: uw_service_icon.
  $styles['uw_service_icon'] = array(
    'label' => 'Service Icon',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 60,
          'height' => 60,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_service_node_info() {
  $items = array(
    'uw_service' => array(
      'name' => t('Service'),
      'base' => 'node_content',
      'description' => t('Service offered at the University of Waterloo'),
      'has_title' => '1',
      'title_label' => t('Service Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
