<?php

/**
 * @file
 * uw_ct_service.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_service_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_service';
  $context->description = 'Displays search for services';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'services' => 'services',
        'services-all' => 'services-all',
        'services-by-audience' => 'services-by-audience',
        'services-popular' => 'services-popular',
        'services-search' => 'services-search',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-aa21ba3d7366fc48e4150f566add9429' => array(
          'module' => 'views',
          'delta' => 'aa21ba3d7366fc48e4150f566add9429',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays search for services');
  $export['uw_service'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_service_audience_category';
  $context->description = 'Displays service categories, all services and by audience';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'services/audience/*' => 'services/audience/*',
        'services/category/*' => 'services/category/*',
        'services-all/*' => 'services-all/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_service-service_tablike_sidebar' => array(
          'module' => 'uw_ct_service',
          'delta' => 'service_tablike_sidebar',
          'region' => 'sidebar_second',
          'weight' => '-11',
        ),
        'views-aa21ba3d7366fc48e4150f566add9429' => array(
          'module' => 'views',
          'delta' => 'aa21ba3d7366fc48e4150f566add9429',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays service categories, all services and by audience');
  $export['uw_service_audience_category'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_service_search_home_sidebar';
  $context->description = 'Displays service search block on the sidebar of home page.';
  $context->tag = 'Content';
  $context->conditions = array(
    'context_var' => array(
      'values' => array(
        'uw_ct_services_search_on_homepage|1' => 'uw_ct_services_search_on_homepage|1',
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-aa21ba3d7366fc48e4150f566add9429' => array(
          'module' => 'views',
          'delta' => 'aa21ba3d7366fc48e4150f566add9429',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays service search block on the sidebar of home page.');
  $export['uw_service_search_home_sidebar'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_service_sidebar_content';
  $context->description = 'Displays service page sidebar content.';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_service' => 'uw_service',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_service-uw_sidebar_block_service' => array(
          'module' => 'uw_ct_service',
          'delta' => 'uw_sidebar_block_service',
          'region' => 'sidebar_second',
          'weight' => '-20',
        ),
        'uw_ct_service-uw_sidebar_related_links_service' => array(
          'module' => 'uw_ct_service',
          'delta' => 'uw_sidebar_related_links_service',
          'region' => 'sidebar_second',
          'weight' => '-19',
        ),
        'views-aa21ba3d7366fc48e4150f566add9429' => array(
          'module' => 'views',
          'delta' => 'aa21ba3d7366fc48e4150f566add9429',
          'region' => 'sidebar_second',
          'weight' => '-17',
        ),
        'uw_ct_service-service_tablike_sidebar' => array(
          'module' => 'uw_ct_service',
          'delta' => 'service_tablike_sidebar',
          'region' => 'sidebar_second',
          'weight' => '-18',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays service page sidebar content.');
  $export['uw_service_sidebar_content'] = $context;

  return $export;
}
