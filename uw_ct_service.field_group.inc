<?php

/**
 * @file
 * uw_ct_service.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_service_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service_hidden|node|uw_service|form';
  $field_group->group_name = 'group_service_hidden';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hidden fields',
    'weight' => '25',
    'children' => array(
      0 => 'field_service_business_critical',
      1 => 'field_service_business_owner',
      2 => 'field_service_contact',
      3 => 'field_service_dependent_services',
      4 => 'field_service_level_options',
      5 => 'field_service_manager',
      6 => 'field_service_owner',
      7 => 'field_service_resources_used',
      8 => 'field_service_sla',
      9 => 'field_service_support_path',
      10 => 'field_service_support_services',
      11 => 'field_service_tags',
      12 => 'field_service_tools',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-service-hidden field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_service_hidden|node|uw_service|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service_hours|node|uw_service|form';
  $field_group->group_name = 'group_service_hours';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Service hours',
    'weight' => '19',
    'children' => array(
      0 => 'field_date_closed',
      1 => 'field_hours_change',
      2 => 'field_hours_notice',
      3 => 'field_service_hours',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-service-hours field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_service_hours|node|uw_service|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service_location|node|uw_service|form';
  $field_group->group_name = 'group_service_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Service location',
    'weight' => '20',
    'children' => array(
      0 => 'field_service_location',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-service-location field-group-fieldset ',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_service_location|node|uw_service|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_service_rt_forms|node|uw_service|form';
  $field_group->group_name = 'group_service_rt_forms';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'RT forms',
    'weight' => '24',
    'children' => array(
      0 => 'field_service_rt_email_address',
      1 => 'field_service_rt_email_checkbox',
      2 => 'field_service_rt_web_checkbox',
      3 => 'field_service_rt_web_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-service-rt-forms field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_service_rt_forms|node|uw_service|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sidebar_relatedlinks|node|uw_service|form';
  $field_group->group_name = 'group_sidebar_relatedlinks';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sidebar related links',
    'weight' => '29',
    'children' => array(
      0 => 'field_related_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Sidebar related links',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_sidebar_relatedlinks|node|uw_service|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sidebar|node|uw_service|form';
  $field_group->group_name = 'group_sidebar';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sidebar complementary content',
    'weight' => '26',
    'children' => array(
      0 => 'field_sidebar_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Sidebar complementary content',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_sidebar|node|uw_service|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_file|node|uw_service|form';
  $field_group->group_name = 'group_upload_file';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload a file',
    'weight' => '8',
    'children' => array(
      0 => 'field_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload a file',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload_file|node|uw_service|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_image|node|uw_service|form';
  $field_group->group_name = 'group_upload_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_service';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image',
    'weight' => '7',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload an image',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload_image|node|uw_service|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Hidden fields');
  t('RT forms');
  t('Service hours');
  t('Service location');
  t('Sidebar complementary content');
  t('Sidebar related links');
  t('Upload a file');
  t('Upload an image');

  return $field_groups;
}
